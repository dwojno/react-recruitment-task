# Recruitment task
The recruitment task is to write a program to present the book purchasing process (downloading book data from API, adding and removing products from the basket, filling in the form with personal data needed to complete the order).

### The application should consist of 3 subpages:
#### 1. Home page
###### A list of functionalities that consists of the minimum requirements for the abovementioned website:
* downloading data from API after entering the application page (**GET** _/book_),
* displaying previously downloaded data in a specific format, i.e. each book should be represented by one block, which should include the book cover, title, author, number of pages and two buttons:
    * `DODAJ DO KOSZYKA`, which will add a specific item to the shopping cart,
    * `ZAMÓW`, which will direct the user to the order summary stage.
###### Additional functionalities for extra score:
- search engine (by title or author of the book),
- pagination (necessary data will be returned from API; method of implementation arbitrary).
#### 2. Shopping cart page
###### A list of functionalities that consists of the minimum requirements for the abovementioned website:
* displaying a list of user-selected books,
* button `DALEJ`, which will direct the user to the order summary stage.
#### 3. Page containing the form needed to place an order
###### A list of functionalities that consists of the minimum requirements for the abovementioned website:
* preparation of a form with fields:
    * name,
    * last name,
    * phone number,
    * email,
    * address,
    * city,
    * ZIP code,
* using react-final-form when working with a form,
* button `ZAMAWIAM I PŁACĘ` - after pressing it, the data will be sent to the endpoint **POST** _/order_.
###### Additional functionalities for extra score:
* form validation.

### A list of **required** technologies while working on the task
* react,
* redux,
* react-router,
* react-final-form,
* redux-saga


### **Additional score** will be given for:
* tests,
* CSS-in-JS or SASS.

### Wireframe suggesting how the project can be carried out
![](./.docs/img/wireframe_1.jpg)
![](./.docs/img/wireframe_2.jpg)
![](./.docs/img/wireframe_3.jpg)
![](./.docs/img/wireframe_4.jpg)

## API Documentation
Documentation in OpenAPI 3 format is available after starting the mock server and visiting the following address: [localhost:3001/docs](http://localhost:3001/docs).

## Launching the development environment
### Docker
The repository includes Node.js configuration in Docker containers. The following command will be useful to run the repository:
```bash
docker-compose up -d
```
You'll find some Docker advice [here](./.docker/README.md).
## Manual
Node.js with Yarn are required. The project requires installing and running the mock API server:
```bash
cd api
cp .env.dist .env # The file may need modification
yarn install
yarn start
```
and the launching an appropriate project - frontend:
```bash
cd front
cp .env.dist .env # The file may need modification
yarn install
yarn start
```
